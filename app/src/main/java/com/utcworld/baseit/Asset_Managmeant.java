package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class Asset_Managmeant extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset__managmeant);
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(Asset_Managmeant.this,softwarePackeg_Home_page.class));
        //customType(Asset_Managmeant.this,"fadein-to-fadeout");

        finish();
    }

    public void assetCartout(View view) {
        Intent intent=new Intent(Asset_Managmeant.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Asset / Rent Management");
        intent.putExtra("prices","395600");
        startActivity(intent);
        //finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(Asset_Managmeant.this,softwarePackeg_Home_page.class));
            //customType(Asset_Managmeant.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
