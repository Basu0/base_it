package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class logodesig extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logodesig);
    }

    public void logoordwe(View view) {
        Intent intent=new Intent(logodesig.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Logo Design");
        intent.putExtra("prices","5000");
        startActivity(intent);
    }

    public void backsoftwarepackeg(View view) {
        finish();
    }
}
