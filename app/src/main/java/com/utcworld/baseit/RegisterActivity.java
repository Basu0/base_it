package com.utcworld.baseit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hbb20.CountryCodePicker;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegisterActivity extends AppCompatActivity {
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    LinearLayout layoutdetails,layoutnumbersend,layoutotpverify;
    private RelativeLayout rlayout,layoutdetailsbt;
    private Animation animation;
    Button RegestionBT;
    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    DatabaseReference reference;
    EditText EmailAddress,Password,Passwordretype,nameValu,Addressvalu,CityValu,Postcodevalu,numberValu;
    private ProgressDialog progressBar;
    AlertDialog alertDialog;
    SweetAlertDialog pDialog;
    TextView timecount,numbershow;
    Pinview pin;
    Button numbersendbt,otpverifyBT;
    private CountryCodePicker codePicker;
    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.bgHeader);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        database=FirebaseDatabase.getInstance();
        reference=database.getReference("UserAccount");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RegestionBT=findViewById(R.id.regestiotonBT);
        layoutdetails=findViewById(R.id.layoutdetals);
        layoutnumbersend=findViewById(R.id.layoutnumbersend);
        layoutotpverify=findViewById(R.id.Layoutotpverify);
        layoutdetailsbt=findViewById(R.id.layoutdetailbtn);
        layoutdetails.setVisibility(View.VISIBLE);

        numbersendbt=findViewById(R.id.OtpSend);
        otpverifyBT=findViewById(R.id.verifiy);
        codePicker=(CountryCodePicker)findViewById(R.id.ccp);

        nameValu=findViewById(R.id.nameEdit);
        Addressvalu=findViewById(R.id.addressEdit);
        CityValu=findViewById(R.id.cityEdit);
        Postcodevalu=findViewById(R.id.postEdit);
        numberValu=findViewById(R.id.NumberEdit);
        numbershow=findViewById(R.id.numbershow);

        pin = (Pinview) findViewById(R.id.pinview);
        pin.setPinBackgroundRes(R.drawable.sample_background);
        pin.setPinHeight(60);
        pin.setPinWidth(60);

        //pin.setInputType(Pinview.InputType.NUMBER);
        //pin.setValue("123456");



        progressBar = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();
        EmailAddress=findViewById(R.id.email_id);
        Password=findViewById(R.id.Password_id);
        Passwordretype=findViewById(R.id.Password_reytpy);
        timecount=findViewById(R.id.timecountid);

        rlayout = findViewById(R.id.rlayout);
        animation = AnimationUtils.loadAnimation(this,R.anim.uptodowndiagonal);
        rlayout.setAnimation(animation);

        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                timecount.setText(""+ millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                timecount.setText("done!");
            }

        }.start();

        boolean wificonneaction;
        boolean mobileconneaction;
        ConnectivityManager connectivityManager=(ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();
        if (networkInfo !=null && networkInfo.isConnected()){
            wificonneaction=networkInfo.getType()==ConnectivityManager.TYPE_WIFI;
            mobileconneaction=networkInfo.getType()==ConnectivityManager.TYPE_MOBILE;

            if (wificonneaction){


            }


            else if (mobileconneaction){



            }



        }else {
            new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText("No Internet Connecation")
                    .show();

        }




        RegestionBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=EmailAddress.getText().toString().trim();
                String password=Password.getText().toString().trim();
                String passwordRetype=Passwordretype.getText().toString().trim();
                String Namevalu=nameValu.getText().toString().trim();
                String addressvalu=Addressvalu.getText().toString().trim();
                String Cityvalu=CityValu.getText().toString().trim();
                String Postvalu=Postcodevalu.getText().toString().trim();


                /*if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                    EmailAddress.setError("Email not valid !");
                    EmailAddress.requestFocus();
                    return;
                }

                if (password.isEmpty()){
                    Password.setError("Password enter");
                    Password.requestFocus();
                    return;
                }
                if (password.length()<6){
                    Password.setError("Minimum Password 6 ");
                    Password.requestFocus();
                    return;

                }
                if (!password.equals(passwordRetype)){

                    Passwordretype.setError("Password Not Match ");
                    Passwordretype.requestFocus();
                }
                if (Namevalu.isEmpty()){
                    nameValu.setError("Name enter");
                    nameValu.requestFocus();
                    return;
                }
                if (addressvalu.isEmpty()){
                    Addressvalu.setError("Address enter");
                    Addressvalu.requestFocus();
                    return;
                }
                if (addressvalu.isEmpty()){
                    Addressvalu.setError("Address enter");
                    Addressvalu.requestFocus();
                    return;
                }

                else {*/

                    layoutdetails.setVisibility(View.GONE);
                    layoutdetailsbt.setVisibility(View.GONE);
                    layoutnumbersend.setVisibility(View.VISIBLE);
                    //RegestionMathod();

                //}
            }
        });

        numbersendbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Phone=numberValu.getText().toString().trim();
                String country=codePicker.getSelectedCountryCode();
                String fullnu=("+"+country+""+Phone);

                if (TextUtils.isEmpty(Phone))
                {
                    new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("enter your phone number first !")
                            .show();
                }
                else {
                    /*pDialog = new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("Please wait Phone Verification ");
                    pDialog.setCancelable(false);
                    pDialog.show();*/
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(fullnu, 60, TimeUnit.SECONDS, RegisterActivity.this, callbacks);
                    layoutnumbersend.setVisibility(View.GONE);
                    layoutotpverify.setVisibility(View.VISIBLE);

                }

                numbershow.setText("Your Number :"+fullnu);

            }
        });

        otpverifyBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pinnumber=pin.getValue();
                if (pinnumber.isEmpty())
                {
                    new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Please write verification code first !")
                            .show();
                }
                else {
                    pDialog = new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                    pDialog.setTitleText("Please wait Phone Verification ");
                    pDialog.setCancelable(false);
                    pDialog.show();
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, pinnumber);
                    signInWithPhoneAuthCredential(credential);
                    pDialog.dismiss();

                }

            }
        });

        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential)
            {

                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e)
            {
                new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Number Already Use")
                        .show();

                //Toast.makeText(RegisterActivity.this, "Invalid Phone Number, Please enter correct phone number with your country code..."+e, Toast.LENGTH_LONG).show();
                //progressBar.dismiss();
                //pDialog.dismiss();


            }

            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token)
            {
                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;


                Toast.makeText(RegisterActivity.this, "Code has been sent, please check and verify...", Toast.LENGTH_SHORT).show();
                //pDialog.dismiss();


            }
        };


    }



    public void RegestionMathod(){
        /*progressBar.setTitle("Registration");
        progressBar.setMessage("Please wait, while we are Account Create");
        //progressBar.setIcon(R.drawable.eegistration24px);
        progressBar.setCanceledOnTouchOutside(false);
        progressBar.show();*/
        pDialog = new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading Please wait");
        pDialog.setCancelable(false);
        pDialog.show();

        String email=EmailAddress.getText().toString().trim();
        String password=Password.getText().toString().trim();
        String passwordRetype=Passwordretype.getText().toString().trim();

       mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                       @Override
                       public void onComplete(@NonNull Task<AuthResult> task) {
                          if (task.isSuccessful()){

                              FirebaseUser user=mAuth.getCurrentUser();
                              String email=user.getEmail();
                              String Uid=user.getUid();
                              String Name=nameValu.getText().toString();
                              String password=Password.getText().toString();
                              String address=Addressvalu.getText().toString();
                              String city=CityValu.getText().toString();
                              String postcod=Postcodevalu.getText().toString().trim();
                              String Phone=numberValu.getText().toString().trim();
                              String country=codePicker.getSelectedCountryCode();
                              String fullnu=("+"+country+""+Phone);
                              HashMap<Object,String> hashMap=new HashMap<>();
                              hashMap.put("Name",Name);
                              hashMap.put("Email",email);
                              hashMap.put("Password",password);
                              hashMap.put("Address",address);
                              hashMap.put("City",city);
                              hashMap.put("Zipcode",postcod);
                              hashMap.put("Number",fullnu);
                              hashMap.put("Uid",Uid);
                              reference.child(Name).setValue(hashMap);

                              pDialog.dismiss();
                              //progressBar.dismiss();
                              new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                      .setTitleText("Good job!")
                                      .setContentText("Your Account Create")
                                      .show();



                          }

                          /*else  {
                              if (task.getException() instanceof FirebaseAuthUserCollisionException){

                                 new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.WARNING_TYPE)
                                          .setTitleText("Already Registration")
                                          .setContentText("Your Account Already Add ! ")
                                          .setConfirmText("Yes")
                                          .show();




                                  pDialog.dismiss();



                                  //Toast.makeText(getApplication(),"Already Registration",Toast.LENGTH_LONG).show();

                              }*/
                              else {
                                  new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                                          .setTitleText("Oops...")
                                          .setContentText("Something went wrong!")
                                          .show();

                                  //Toast.makeText(getApplication(),"Error :"+ task.getException().getMessage(),Toast.LENGTH_LONG).show();
                                  pDialog.dismiss();
                              }
                          }


                       //}
                   });



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void signInWithPhoneAuthCredential(final PhoneAuthCredential credential){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            pDialog.dismiss();
                            RegestionMathod();
                            /*new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Good Job")
                                    .setContentText("verification Successfull")
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {

                                        }
                                    })
                                    .show();*/

                        }
                        else {

                            new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText("Invalidation Code")
                                    .setConfirmText("Cancel")
                                    .show();
                        }
                        if (credential!=null){
                            if (credential.getSmsCode() != null) {
                                pin.setValue(credential.getSmsCode());
                                //pDialog.dismiss();

                            }

                        }


                    }
                });
    }



}
