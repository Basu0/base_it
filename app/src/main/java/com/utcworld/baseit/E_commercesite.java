package com.utcworld.baseit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import static android.graphics.text.LineBreaker.JUSTIFICATION_MODE_INTER_WORD;

public class E_commercesite extends AppCompatActivity {
    String webaddress="https://www.baseit.com.bd/e-commerce-development/";
    WebView webView;
    FrameLayout frameLayout;
    ProgressBar progressBar;
    LinearLayout layoutwb,layoutapp;
    Toolbar tolbar1,toolbar2;
    TextView textViewcontant;
    @SuppressLint("WrongConstant")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e_commercesite);

        webView = (WebView) findViewById(R.id.webviewid);
        frameLayout = (FrameLayout) findViewById(R.id.framlaout);
        progressBar = (ProgressBar) findViewById(R.id.progressbaar);
        layoutwb=findViewById(R.id.layoutweb);
        layoutapp=findViewById(R.id.layoutapps);
        tolbar1=findViewById(R.id.toolbarmain);
        toolbar2=findViewById(R.id.toolbarweb);
        textViewcontant=findViewById(R.id.textcontant);
        textViewcontant.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        assert webView != null;
        layoutapp.setVisibility(View.VISIBLE);

        WebSettings webSettings = webView.getSettings();
        webSettings.setAllowFileAccess(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setJavaScriptEnabled(true);
    }

    public void websitego(View view) {
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                tolbar1.setVisibility(View.GONE);
                toolbar2.setVisibility(View.VISIBLE);
                layoutwb.setVisibility(View.VISIBLE);
                layoutapp.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                progressBar.setProgress(newProgress);
                setTitle("Loading Please Wait.... ");
                if (newProgress==100){
                    frameLayout.setVisibility(View.GONE);
                    setTitle(view.getTitle());

                }
                super.onProgressChanged(view, newProgress);
            }




        });
        if (havenetworkConnecation()){
            webView.loadUrl(webaddress);

        }
        else {

            Toast.makeText(this, "No Internet Connecation ... ", Toast.LENGTH_SHORT).show();

        }

        progressBar.setProgress(0);
    }



    public void MANGOCart(View view) {
        Intent intent=new Intent(E_commercesite.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","E-commerce website -MANGO package");
        intent.putExtra("prices","75000");
        startActivity(intent);

    }

    public void PINEAPPLECart(View view) {
        Intent intent=new Intent(E_commercesite.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","E-commerce website -PINEAPPLE package");
        intent.putExtra("prices","100000");
        startActivity(intent);
    }

    public void UNLIMITEDCart(View view) {
        Intent intent=new Intent(E_commercesite.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","E-commerce website -UNLIMITED package");
        intent.putExtra("prices","150000");
        startActivity(intent);
    }


    public void CHERRYcart(View view) {
        Intent intent=new Intent(E_commercesite.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","E-commerce website -CHERRY package");
        intent.putExtra("prices","50000");
        startActivity(intent);
    }

    private boolean havenetworkConnecation(){
        boolean haveconnectionWifi=false;
        boolean haveconnectionMobile=false;
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos=cm.getAllNetworkInfo();
        for (NetworkInfo ni: networkInfos){
            if (ni.getTypeName().equalsIgnoreCase("WiFi"))
                if (ni.isConnected())
                    haveconnectionWifi=true;
            if (ni.getTypeName().equalsIgnoreCase("Mobile"))
                if (ni.isConnected())
                    haveconnectionMobile=true;
        }
        return haveconnectionWifi || haveconnectionMobile;

    }

    public void backsoftwarepackeg(View view) {
        toolbar2.setVisibility(View.GONE);
        tolbar1.setVisibility(View.VISIBLE);
        layoutwb.setVisibility(View.GONE);
        layoutapp.setVisibility(View.VISIBLE);

    }

    public void toolbarmin(View view) {
        finish();
    }
}
