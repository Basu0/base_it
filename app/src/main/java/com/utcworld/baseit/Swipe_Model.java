package com.utcworld.baseit;

import android.widget.Button;

public class Swipe_Model {
    private int image;
    private String headname;
    private String pricetext;
    private String descripstion;
    private String DC1T;
    private String DC2T;
    private String DC3T;
    private String DC4T;
    private String TC1T;
    private String TC4T;
    private String TC6T;

    public Swipe_Model(int image, String headname, String pricetext, String descripstion,String DC1T,String DC2T,String DC3T,String DC4T,String TC1T,String TC4T,String TC6T) {
        this.image = image;
        this.headname = headname;
        this.pricetext = pricetext;
        this.descripstion = descripstion;
        this.DC1T = DC1T;
        this.DC2T = DC2T;
        this.DC3T = DC3T;
        this.DC4T = DC4T;
        this.TC1T = TC1T;
        this.TC4T = TC4T;
        this.TC6T = TC6T;
    }

    public String getTC1T() {
        return TC1T;
    }

    public void setTC1T(String TC1T) {
        this.TC1T = TC1T;
    }

    public String getTC4T() {
        return TC4T;
    }

    public void setTC4T(String TC4T) {
        this.TC4T = TC4T;
    }

    public String getTC6T() {
        return TC6T;
    }

    public void setTC6T(String TC6T) {
        this.TC6T = TC6T;
    }

    public String getDC1T() {
        return DC1T;
    }

    public void setDC1T(String DC1T) {
        this.DC1T = DC1T;
    }

    public String getDC2T() {
        return DC2T;
    }

    public void setDC2T(String DC2T) {
        this.DC2T = DC2T;
    }

    public String getDC3T() {
        return DC3T;
    }

    public void setDC3T(String DC3T) {
        this.DC3T = DC3T;
    }

    public String getDC4T() {
        return DC4T;
    }

    public void setDC4T(String DC4T) {
        this.DC4T = DC4T;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getHeadname() {
        return headname;
    }

    public void setHeadname(String headname) {
        this.headname = headname;
    }

    public String getPricetext() {
        return pricetext;
    }

    public void setPricetext(String pricetext) {
        this.pricetext = pricetext;
    }

    public String getDescripstion() {
        return descripstion;
    }

    public void setDescripstion(String descripstion) {
        this.descripstion = descripstion;
    }
}