package com.utcworld.baseit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import static android.graphics.text.LineBreaker.JUSTIFICATION_MODE_INTER_WORD;
import static maes.tech.intentanim.CustomIntent.customType;

public class Domain_regestioton extends AppCompatActivity {
    TextView textViewcontant;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_domain_regestioton);
        textViewcontant=findViewById(R.id.textcontant);
        textViewcontant.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(Domain_regestioton.this,Baseit_Home.class));
        //customType(Domain_regestioton.this,"bottom-to-up");

        finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(Domain_regestioton.this,Baseit_Home.class));
            //customType(Domain_regestioton.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
