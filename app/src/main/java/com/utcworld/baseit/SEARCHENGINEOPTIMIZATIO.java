package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

public class SEARCHENGINEOPTIMIZATIO extends AppCompatActivity {
    String webaddress="https://www.baseit.com.bd/search-engine-optimization/";
    WebView webView;
    FrameLayout frameLayout;
    ProgressBar progressBar;
    LinearLayout layoutwb,layoutapp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchengineoptimizatio);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        webView = (WebView) findViewById(R.id.webviewid);
        frameLayout = (FrameLayout) findViewById(R.id.framlaout);
        progressBar = (ProgressBar) findViewById(R.id.progressbaar);
        layoutwb=findViewById(R.id.layoutweb);
        layoutapp=findViewById(R.id.layoutapps);
        assert webView != null;
        layoutapp.setVisibility(View.VISIBLE);

        WebSettings webSettings = webView.getSettings();
        webSettings.setAllowFileAccess(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setJavaScriptEnabled(true);





    }
    public void backsoftwarepackeg(View view) {
        finish();
    }

    public void sitego(View view) {
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                layoutwb.setVisibility(View.VISIBLE);
                layoutapp.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                progressBar.setProgress(newProgress);
                setTitle("Loading Please Wait.... ");
                if (newProgress==100){
                    frameLayout.setVisibility(View.GONE);
                    setTitle(view.getTitle());

                }
                super.onProgressChanged(view, newProgress);
            }




        });
        if (havenetworkConnecation()){
            webView.loadUrl(webaddress);

        }
        else {

            Toast.makeText(this, "No Internet Connecation ... ", Toast.LENGTH_SHORT).show();

        }

        progressBar.setProgress(0);

    }

    public void m500border(View view) {
        Intent intent=new Intent(SEARCHENGINEOPTIMIZATIO.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Search Engine Optimization (at a time payment) 6*15,000=90,000 BDT.");
        intent.putExtra("prices","90000");
        startActivity(intent);
    }


    private boolean havenetworkConnecation(){
        boolean haveconnectionWifi=false;
        boolean haveconnectionMobile=false;
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos=cm.getAllNetworkInfo();
        for (NetworkInfo ni: networkInfos){
            if (ni.getTypeName().equalsIgnoreCase("WiFi"))
                if (ni.isConnected())
                    haveconnectionWifi=true;
            if (ni.getTypeName().equalsIgnoreCase("Mobile"))
                if (ni.isConnected())
                    haveconnectionMobile=true;
        }
        return haveconnectionWifi || haveconnectionMobile;

    }


    public void serachorder(View view) {
        Intent intent=new Intent(SEARCHENGINEOPTIMIZATIO.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Search Engine Optimization");
        intent.putExtra("prices","20000");
        startActivity(intent);
    }
}
