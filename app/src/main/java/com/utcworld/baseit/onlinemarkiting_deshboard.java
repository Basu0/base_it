package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class onlinemarkiting_deshboard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onlinemarkiting_deshboard);
    }

    public void blingpageback(View view) {
        finish();
    }

    public void emailmarketingpage(View view) {
        startActivity(new Intent(onlinemarkiting_deshboard.this,emailmarkiting_page.class));
        customType(onlinemarkiting_deshboard.this,"fadein-to-fadeout");
    }

    public void smsmarketing(View view) {
        startActivity(new Intent(onlinemarkiting_deshboard.this,smsmarkiting.class));
        customType(onlinemarkiting_deshboard.this,"fadein-to-fadeout");
    }

    public void seopagego(View view) {
        startActivity(new Intent(onlinemarkiting_deshboard.this,SEARCHENGINEOPTIMIZATIO.class));
        customType(onlinemarkiting_deshboard.this,"fadein-to-fadeout");
    }
}
