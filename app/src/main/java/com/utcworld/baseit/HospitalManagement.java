package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class HospitalManagement extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_management);
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(HospitalManagement.this,softwarePackeg_Home_page.class));
        //customType(HospitalManagement.this,"fadein-to-fadeout");

        finish();
    }

    public void hospitalCartout(View view) {
        Intent intent=new Intent(HospitalManagement.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Hospital Management");
        intent.putExtra("prices","395000");
        startActivity(intent);
        //finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(HospitalManagement.this,softwarePackeg_Home_page.class));
            //customType(HospitalManagement.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
