package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class DedicatedHosting extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dedicated_hosting);
    }

    public void m500border(View view) {
        Intent intent=new Intent(DedicatedHosting.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Dedicated Hosting (500 MB)");
        intent.putExtra("prices","5000");
        startActivity(intent);
    }

    public void m1border(View view) {
        Intent intent=new Intent(DedicatedHosting.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Dedicated Hosting (1 GB)");
        intent.putExtra("prices","9000");
        startActivity(intent);
    }

    public void m2border(View view) {
        Intent intent=new Intent(DedicatedHosting.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Dedicated Hosting (2 GB)");
        intent.putExtra("prices","16000");
        startActivity(intent);
    }

    public void m3border(View view) {
        Intent intent=new Intent(DedicatedHosting.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Dedicated Hosting (3 GB)");
        intent.putExtra("prices","23000 ");
        startActivity(intent);
    }

    public void m5border(View view) {
        Intent intent=new Intent(DedicatedHosting.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Dedicated Hosting (5 GB)");
        intent.putExtra("prices","35000");
        startActivity(intent);
    }

    public void m10border(View view) {
        Intent intent=new Intent(DedicatedHosting.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Dedicated Hosting (10 GB)");
        intent.putExtra("prices","60000");
        startActivity(intent);
    }

    public void websitego(View view) {
        Intent openURL = new Intent(android.content.Intent.ACTION_VIEW);
        openURL.setData(Uri.parse("https://www.baseit.com.bd/dedicate-hosting-server/"));
        startActivity(openURL);
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(DedicatedHosting.this,Baseit_Home.class));
        //customType(DedicatedHosting.this,"fadein-to-fadeout");

        finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(DedicatedHosting.this,Baseit_Home.class));
            //customType(DedicatedHosting.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
