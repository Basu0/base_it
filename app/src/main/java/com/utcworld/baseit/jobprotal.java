package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class jobprotal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobprotal);
    }

    public void jopprotancart(View view) {
        Intent intent=new Intent(jobprotal.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Job Portal");
        intent.putExtra("prices","450000");
        startActivity(intent);
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(jobprotal.this,Baseit_Home.class));
        //customType(jobprotal.this,"fadein-to-fadeout");
        finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            // startActivity(new Intent(cart_design_And_Chackout.this,inventorysoftwarepackeg.class));
            //customType(cart_design_And_Chackout.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
