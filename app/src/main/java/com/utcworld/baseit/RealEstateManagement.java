package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class RealEstateManagement extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_estate_management);
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(RealEstateManagement.this,softwarePackeg_Home_page.class));
        //customType(RealEstateManagement.this,"fadein-to-fadeout");

        finish();
    }

    public void realestateCartout(View view) {
        Intent intent=new Intent(RealEstateManagement.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Real Estate Management");
        intent.putExtra("prices","495000");
        startActivity(intent);
        //finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(RealEstateManagement.this,softwarePackeg_Home_page.class));
            //customType(RealEstateManagement.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
