package com.utcworld.baseit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static android.graphics.text.LineBreaker.JUSTIFICATION_MODE_INTER_WORD;

public class aboutus extends AppCompatActivity {
    TextView textViewcontant1,textViewcontant12;
    @SuppressLint("WrongConstant")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        textViewcontant1=findViewById(R.id.textcontant1);
        textViewcontant12=findViewById(R.id.textcontant2);
        textViewcontant1.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        textViewcontant12.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
    }

    public void backsoftwarepackeg(View view) {
        finish();
    }
}
