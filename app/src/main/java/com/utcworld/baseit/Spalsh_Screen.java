package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.flaviofaria.kenburnsview.KenBurnsView;
import com.flaviofaria.kenburnsview.RandomTransitionGenerator;
import com.flaviofaria.kenburnsview.Transition;

public class Spalsh_Screen extends AppCompatActivity {
    LinearLayout l1,l2;
    Button btnsub;
    private KenBurnsView kbv;
    Animation uptodown,downtoup;
    private boolean moving = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh__screen);
        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        //making activity full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_spalsh__screen);
        //l1 = (LinearLayout) findViewById(R.id.l1);
        //l2 = (LinearLayout) findViewById(R.id.l2);
        //uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodown);
        //downtoup = AnimationUtils.loadAnimation(this,R.anim.downtoup);
        //l1.setAnimation(uptodown);
        //l2.setAnimation(downtoup);
        kbv = findViewById(R.id.kb);

        int t=3000;
        final Intent intent=new Intent(Spalsh_Screen.this,Baseit_Home.class);


        Thread thread=new Thread(){
            public void run(){

                try {
                    sleep(4000);
                }catch (InterruptedException e){
                    e.printStackTrace();

                }
                finally {
                    startActivity(intent);
                    finish();
                }
            }

        };
        thread.start();

        AccelerateDecelerateInterpolator adi = new AccelerateDecelerateInterpolator();
        RandomTransitionGenerator generator = new RandomTransitionGenerator(2000, adi);
        kbv.setTransitionGenerator(generator);

        kbv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (moving) {
                    kbv.pause();
                    moving = false;
                } else {
                    kbv.resume();
                    moving = true;
                }
            }
        });


    }


}