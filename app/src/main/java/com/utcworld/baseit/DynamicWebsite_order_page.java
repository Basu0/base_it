package com.utcworld.baseit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.animation.ArgbEvaluator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.graphics.text.LineBreaker.JUSTIFICATION_MODE_INTER_WORD;
import static maes.tech.intentanim.CustomIntent.customType;

public class DynamicWebsite_order_page extends AppCompatActivity {
  TextView textViewcontant;
    ViewPager viewPager;
    Swipe_Adapter adapter;
    List<Swipe_Model> models;
    Integer[] colors = null;
    String name=null;
    Button orderBt;
    ArgbEvaluator argbEvaluator = new ArgbEvaluator();

    LinearLayout linearLayout;

    @SuppressLint("WrongConstant")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_website_order_page);
        textViewcontant=findViewById(R.id.textcontant);
        textViewcontant.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        linearLayout=findViewById(R.id.layoutbackground);



        models = new ArrayList<>();
        models.add(new Swipe_Model(R.drawable.appleicon,"APPLE", "25,000tk / $320","Dynamic Page no: 1-09 pages (Including menu and submenu)","1 GB Hosting under Linux Server for 1 year.","15 Email address","Domain .com .net .org .info Registration","3000 MB Monthly Data transfer / Traffic.","Development Timelines (If content is available): 10 working days.","Charges per extra pages: 1000 Taka.","Renew: 7000 taka per year for Domain & Hosting."));
        models.add(new Swipe_Model(R.drawable.orangeicon,"ORANGE", "40,000tk / $500","Dynamic Page no: 10-19 pages (Including menu and submenu)","Domain .com .net .org .info Registration.","2 GB Hosting under Linux Server for 1 year.","50 Email address","5000 MB Monthly Data transfer / Traffic.","Development Timelines (If content is available): 15 working days.","Charges per extra pages: 1000 Taka.","Renew: 12,000 taka per year for Domain & Hosting."));
        models.add(new Swipe_Model(R.drawable.strawberry,"STRAWBERRY", "70,000tk / $875","Dynamic Page no: 20-30 pages (Including menu and submenu)","Domain .com .net .org .info Registration.","4 GB Hosting under Linux Server for 1 year.","50 Email address","10,000 MB Monthly Data transfer / Traffic.","Development Timelines (If content is available): 20 working days.","Charges per extra pages: 1000 Taka.","Renew: 20,000 taka per year for Domain & Hosting."));
        models.add(new Swipe_Model(R.drawable.unlimiteddata,"UNLIMITED", "1,60,000tk / $2000","Dynamic Page no: Unlimited pages (Including menu and submenu)","Domain .com .net .org .info Registration.","5 GB Hosting under Linux Server for 1 year.","100 Email address","20,000 MB Monthly Data transfer / Traffic.","Development Timelines (If content is available): 20 working days.","Charges per extra pages: 2000 Taka.","Renew: 20,000 taka per year for Domain & Hosting."));

        adapter = new Swipe_Adapter(models, this);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
        viewPager.setPadding(130, 0, 130, 0);




        final Integer[] colors_temp = {
                getResources().getColor(R.color.color1),
                getResources().getColor(R.color.color2),
                getResources().getColor(R.color.color3),
                getResources().getColor(R.color.color4)
        };



        colors = colors_temp;


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position < (adapter.getCount() -1) && position < (colors.length - 1)) {
                    linearLayout.setBackgroundColor(

                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    colors[position],
                                    colors[position + 1]

                            )


                    );

                    viewPager.setBackgroundColor(

                            (Integer) argbEvaluator.evaluate(
                                    positionOffset,
                                    colors[position],
                                    colors[position + 1]
                            )
                    );

                }


                else {
                    linearLayout.setBackgroundColor(colors[colors.length - 1]);
                    viewPager.setBackgroundColor(colors[colors.length - 1]);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }        });



    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(DynamicWebsite_order_page.this,Baseit_Home.class));
            //customType(DynamicWebsite_order_page.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void backsoftwarepackeg(View view) {
        finish();
    }
}
