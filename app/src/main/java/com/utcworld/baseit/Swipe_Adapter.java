package com.utcworld.baseit;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import java.util.List;

public class Swipe_Adapter extends PagerAdapter {
    private List<Swipe_Model> models;
    private LayoutInflater layoutInflater;
    private Context context;

    public Swipe_Adapter(List<Swipe_Model> models, Context context) {
        this.models = models;
        this.context = context;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item, container, false);
        TextView headname, pricetext,descripstion,DC1T,DC2T,DC3T,DC4T,TC1T,TC4T,TC6T;
        ImageView imageView;

        headname = view.findViewById(R.id.headname);
        pricetext = view.findViewById(R.id.pricetext);
        descripstion=view.findViewById(R.id.descripstion);
        imageView = view.findViewById(R.id.image);
        DC1T=view.findViewById(R.id.domaiC1);
        DC2T=view.findViewById(R.id.domaiC2);
        DC3T=view.findViewById(R.id.domaiC3);
        DC4T=view.findViewById(R.id.domaiC4);

        TC1T=view.findViewById(R.id.TC1T);
        TC4T=view.findViewById(R.id.TC4T);
        TC6T=view.findViewById(R.id.TC6T);


        headname.setText(models.get(position).getHeadname());
        pricetext.setText(models.get(position).getPricetext());
        descripstion.setText(models.get(position).getDescripstion());
        imageView.setImageResource(models.get(position).getImage());
        DC1T.setText(models.get(position).getDC1T());
        DC2T.setText(models.get(position).getDC2T());
        DC3T.setText(models.get(position).getDC3T());
        DC4T.setText(models.get(position).getDC4T());

        TC1T.setText(models.get(position).getTC1T());
        TC4T.setText(models.get(position).getTC4T());
        TC6T.setText(models.get(position).getTC6T());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, cart_design_And_Chackout.class);
               String Packegname=models.get(position).getHeadname();
                //intent.putExtra("param", models.get(position).getHeadname());
                if(Packegname.equals("APPLE")){
                intent.putExtra("softwarename","Dynamic Website – "+models.get(position).getHeadname()+" Package");
                intent.putExtra("prices","25000");
                context.startActivity(intent);
                }
                if(Packegname.equals("ORANGE")){
                    intent.putExtra("softwarename","Dynamic Website – "+models.get(position).getHeadname()+" Package");
                    intent.putExtra("prices","40000");
                    context.startActivity(intent);
                }
                if(Packegname.equals("STRAWBERRY")){
                    intent.putExtra("softwarename","Dynamic Website – "+models.get(position).getHeadname()+" Package");
                    intent.putExtra("prices","70000");
                    context.startActivity(intent);
                }
                if(Packegname.equals("UNLIMITED")){
                    intent.putExtra("softwarename","Dynamic Website – "+models.get(position).getHeadname()+" Package");
                    intent.putExtra("prices","160000");
                    context.startActivity(intent);
                }
                // finish();
            }
        });

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
