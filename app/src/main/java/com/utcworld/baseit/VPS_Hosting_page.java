package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class VPS_Hosting_page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vps__hosting_page);
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(VPS_Hosting_page.this,Baseit_Home.class));
        //customType(VPS_Hosting_page.this,"fadein-to-fadeout");

        finish();
    }

    public void websitego(View view) {
        Intent openURL = new Intent(android.content.Intent.ACTION_VIEW);
        openURL.setData(Uri.parse("https://www.baseit.com.bd/vps-hosting-server/"));
        startActivity(openURL);
    }

    public void m500border(View view) {
        Intent intent=new Intent(VPS_Hosting_page.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","VPS Hosting (500MB)");
        intent.putExtra("prices","3000");
        startActivity(intent);

    }

    public void m1border(View view) {
        Intent intent=new Intent(VPS_Hosting_page.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","VPS Hosting (1 GB)");
        intent.putExtra("prices","5000");
        startActivity(intent);
    }

    public void m2border(View view) {
        Intent intent=new Intent(VPS_Hosting_page.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","VPS Hosting (2 GB)");
        intent.putExtra("prices","10000");
        startActivity(intent);
    }

    public void m3border(View view) {
        Intent intent=new Intent(VPS_Hosting_page.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","VPS Hosting (3 GB)");
        intent.putExtra("prices","15000 ");
        startActivity(intent);
    }

    public void m5border(View view) {
        Intent intent=new Intent(VPS_Hosting_page.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","VPS Hosting (5 GB)");
        intent.putExtra("prices","23000");
        startActivity(intent);
    }

    public void m10border(View view) {
        Intent intent=new Intent(VPS_Hosting_page.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","VPS Hosting (10 GB)");
        intent.putExtra("prices","40000");
        startActivity(intent);
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(VPS_Hosting_page.this,Baseit_Home.class));
            //customType(VPS_Hosting_page.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
