package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class softwarePackeg_Home_page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_software_packeg__home_page);
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(softwarePackeg_Home_page.this,Baseit_Home.class));
            //customType(softwarePackeg_Home_page.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void softwarepackegopen(View view) {
        startActivity(new Intent(softwarePackeg_Home_page.this,inventorysoftwarepackeg.class));
        customType(softwarePackeg_Home_page.this,"fadein-to-fadeout");
        //finish();
    }

    public void accountingpage(View view) {
        startActivity(new Intent(softwarePackeg_Home_page.this,accountingsoftware.class));
        customType(softwarePackeg_Home_page.this,"fadein-to-fadeout");
        //finish();
    }

    public void realEstatesoftware(View view) {
        startActivity(new Intent(softwarePackeg_Home_page.this,RealEstateManagement.class));
        customType(softwarePackeg_Home_page.this,"fadein-to-fadeout");
        //finish();
    }

    public void hrpayroll(View view) {
        startActivity(new Intent(softwarePackeg_Home_page.this,hrpayroll_Managment.class));
        customType(softwarePackeg_Home_page.this,"fadein-to-fadeout");
        //finish();
    }

    public void garmentspage(View view) {
        startActivity(new Intent(softwarePackeg_Home_page.this,garments_Management.class));
        customType(softwarePackeg_Home_page.this,"fadein-to-fadeout");
        //finish();
    }

    public void hospitalmamangement(View view) {
        startActivity(new Intent(softwarePackeg_Home_page.this,HospitalManagement.class));
        customType(softwarePackeg_Home_page.this,"fadein-to-fadeout");
        //finish();
    }

    public void asstsoftware(View view) {
        startActivity(new Intent(softwarePackeg_Home_page.this,Asset_Managmeant.class));
        customType(softwarePackeg_Home_page.this,"fadein-to-fadeout");
        //finish();
    }

    public void blingpageback(View view) {
        //startActivity(new Intent(softwarePackeg_Home_page.this,Baseit_Home.class));
        //customType(softwarePackeg_Home_page.this,"fadein-to-fadeout");
        finish();
    }
}
