package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class garments_Management extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_garments__management);
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(garments_Management.this,softwarePackeg_Home_page.class));
        //customType(garments_Management.this,"fadein-to-fadeout");

        finish();
    }

    public void garmentsCartout(View view) {
        Intent intent=new Intent(garments_Management.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Garments ERP Management");
        intent.putExtra("prices","5050000  ");
        startActivity(intent);
        //finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(garments_Management.this,softwarePackeg_Home_page.class));
            //customType(garments_Management.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
