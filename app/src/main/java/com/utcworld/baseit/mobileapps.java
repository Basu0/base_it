package com.utcworld.baseit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static android.graphics.text.LineBreaker.JUSTIFICATION_MODE_INTER_WORD;

public class mobileapps extends AppCompatActivity {
    TextView textViewcontant1,textViewcontant12;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobileapps);
        textViewcontant1=findViewById(R.id.textcontant1);
        textViewcontant12=findViewById(R.id.textcontant21);
        textViewcontant1.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        textViewcontant12.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
    }

    public void backsoftwarepackeg(View view) {
        finish();
    }

    public void andridorder(View view) {
        Intent intent=new Intent(mobileapps.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Andriod Apps");
        intent.putExtra("prices","70000");
        startActivity(intent);
    }

    public void iponeapps(View view) {
        Intent intent=new Intent(mobileapps.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","iPhone Apps");
        intent.putExtra("prices","100000");
        startActivity(intent);
    }
}
