package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class hrpayroll_Managment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hrpayroll__managment);
    }

    public void hrpayrollCartout(View view) {
        Intent intent=new Intent(hrpayroll_Managment.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","HR / Payroll Management");
        intent.putExtra("prices","420000 ");
        startActivity(intent);
        //finish();
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(hrpayroll_Managment.this,softwarePackeg_Home_page.class));
        //customType(hrpayroll_Managment.this,"fadein-to-fadeout");

        finish();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(hrpayroll_Managment.this,softwarePackeg_Home_page.class));
            //customType(hrpayroll_Managment.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
