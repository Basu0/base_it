package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.LayoutInflaterCompat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class inventorysoftwarepackeg extends AppCompatActivity {
  Spinner spinnerCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventorysoftwarepackeg);
        spinnerCart=findViewById(R.id.cartSpneer);
        List<String> catagory=new ArrayList<>();
        catagory.add("Bangladeshi Taka");
        catagory.add("USD");
        //ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.simpel_speenar_layout,R.id.texview_sampel_speenar,catagory);
        //spinnerCart.setAdapter(adapter);



    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(inventorysoftwarepackeg.this,softwarePackeg_Home_page.class));
        //customType(inventorysoftwarepackeg.this,"fadein-to-fadeout");

        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(inventorysoftwarepackeg.this,softwarePackeg_Home_page.class));
            //customType(inventorysoftwarepackeg.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void CartOrderBt(View view) {
        CartOrderAlart();

    }

    private void CartOrderAlart() {
        //startActivity(new Intent(inventorysoftwarepackeg.this,cart_design_And_Chackout.class));
        //customType(inventorysoftwarepackeg.this,"fadein-to-fadeout");
        Intent intent=new Intent(inventorysoftwarepackeg.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Inventory Management");
        intent.putExtra("prices","450000");
        startActivity(intent);
        //finish();
    }
}
