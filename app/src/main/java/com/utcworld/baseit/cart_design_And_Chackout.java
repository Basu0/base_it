package com.utcworld.baseit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.hbb20.CountryCodePicker;

import java.util.ArrayList;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class cart_design_And_Chackout extends AppCompatActivity {
   Spinner Cartspinner;
   TextView namesetSoftware,apps_price,totalprice,amounticon1,amounticon2,namesetbillingad,priceanddollarset;
   TextView amoutbllingset1,amoutbllingset2,amoutbllingset3,amounticonbli1,amounticonbli2,amounticonbli3;
   public Double price;
   public String softwarename;
    int counter = 0;
    TextView quentityvalu;
    private CountryCodePicker codePicker;
    LinearLayout layout1,layout2;
    Animation uptodown,downtoup,getUptodown,getDowntoup;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_design__and__chackout);

        Cartspinner =findViewById(R.id.cartSpneer);
        namesetSoftware=findViewById(R.id.softwarenameset);
        apps_price=findViewById(R.id.apps_price);
        totalprice=findViewById(R.id.totalprice);
        quentityvalu=findViewById(R.id.quentityvalu);
        amounticon1=findViewById(R.id.amounticon1);
        amounticon2=findViewById(R.id.amounticon2);
        codePicker=(CountryCodePicker)findViewById(R.id.ccp);
        namesetbillingad=findViewById(R.id.namesetbillingadress);
        amoutbllingset1=findViewById(R.id.amoutbling1);
        amoutbllingset2=findViewById(R.id.amoutbling2);
        amoutbllingset3=findViewById(R.id.amoutbling3);
        amounticonbli1=findViewById(R.id.amounticonbli1);
        amounticonbli2=findViewById(R.id.amounticonbli2);
        amounticonbli3=findViewById(R.id.amounticonbli3);
        priceanddollarset=findViewById(R.id.priceandDollarset);



        layout1=findViewById(R.id.layou1);
        layout2=findViewById(R.id.layou2);


        uptodown = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        downtoup = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        layout1.setAnimation(uptodown);
        layout1.setVisibility(View.VISIBLE);
        layout2.setVisibility(View.GONE);
        List<String> catagory=new ArrayList<>();
        catagory.add("Bangladeshi Taka");
        catagory.add("USD");
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.simpel_speenar_layout,R.id.texview_sampel_speenar,catagory);
        Cartspinner.setAdapter(adapter);

        String country=codePicker.getSelectedCountryName();
        counter = counter + 1;
        Cartspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String Text = adapterView.getSelectedItem().toString();
                if(Text.equals("Bangladeshi Taka")) {
                    quentityvalu.setText(String.valueOf(counter));
                    Double totalQw= counter * price ;
                    apps_price.setText(totalQw.toString());
                    totalprice.setText(totalQw.toString());
                    amounticon1.setText("৳");
                    amounticon2.setText("৳");


                }
                if(Text.equals("USD")) {
                    //incrimentmethod();
                   Double price=Double.parseDouble(apps_price.getText().toString());
                   Double valusprice=price / 83.33;
                    double number2 = (float)(Math.round(valusprice * 100))/100.0;
                   apps_price.setText(String.valueOf(number2));
                    totalprice.setText(String.valueOf(number2));
                    amounticon1.setText("$");
                    amounticon2.setText("$");

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
             softwarename = extras.getString("softwarename");
             price = Double.valueOf(extras.getString("prices"));
             namesetSoftware.setText(softwarename);
            apps_price.setText(price.toString());
            totalprice.setText(price.toString());

            //The key argument here must match that used in the other activity
        }
        Double totalQw= price /83.33 ;
        double number2 = (int)(Math.round(totalQw * 100))/100.0;
        priceanddollarset.setText("Price ৳ "+price+" / $ "+number2+"");

    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
           // startActivity(new Intent(cart_design_And_Chackout.this,inventorysoftwarepackeg.class));
            //customType(cart_design_And_Chackout.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void valuIncrimentBT(View view) {
        incrimentmethod();

    }

    public void valuDecrement(View view) {
        dicrementMethod();
    }

    public void incrimentmethod(){

        String ProductItem=Cartspinner.getSelectedItem().toString();
        if(ProductItem.equals("Bangladeshi Taka")) {
        counter = counter + 1;
        quentityvalu.setText(String.valueOf(counter));
        Double totalQw= counter * price ;

        apps_price.setText(totalQw.toString());
        totalprice.setText(totalQw.toString());
        }

        if(ProductItem.equals("USD")) {
            counter = counter + 1;
            quentityvalu.setText(String.valueOf(counter));
            Double totalQw= counter * price /83.33 ;
            double number2 = (int)(Math.round(totalQw * 100))/100.0;
            apps_price.setText(String.valueOf(number2));
            totalprice.setText(String.valueOf(number2));
        }
    }
    public void dicrementMethod(){
        String ProductItem=Cartspinner.getSelectedItem().toString();
        counter = counter - 1;
        if(ProductItem.equals("Bangladeshi Taka")) {
            quentityvalu.setText(String.valueOf(counter));
            Double totalQw= counter * price ;
            apps_price.setText(totalQw.toString());
            totalprice.setText(totalQw.toString());
        }

        if(ProductItem.equals("USD")) {
            counter = counter - 1;
            quentityvalu.setText(String.valueOf(counter));
            Double totalQw= counter * price /83.33 ;
            double number2 = (int)(Math.round(totalQw * 100))/100.0;
            apps_price.setText(String.valueOf(number2));
            totalprice.setText(String.valueOf(number2));
        }
    }

    public void cartorderComplete(View view) {
        layout1.setVisibility(View.GONE);
        layout2.setVisibility(View.VISIBLE);
        String appsTotalprice=apps_price.getText().toString();
        String amounticon=amounticon1.getText().toString();
        amounticonbli1.setText(amounticon);
        amounticonbli2.setText(amounticon);
        amounticonbli3.setText(amounticon);

        namesetbillingad.setText(""+softwarename);
        amoutbllingset1.setText(""+appsTotalprice);
        amoutbllingset2.setText(""+appsTotalprice);
        amoutbllingset3.setText(""+appsTotalprice);
    }

    public void blingpageback(View view) {
        layout1.setAnimation(downtoup);
        layout1.setVisibility(View.VISIBLE);
        layout2.setVisibility(View.GONE);
    }

    public void backinventorypage(View view) {

        /*if (softwarename.equals("Inventory Management")){
            startActivity(new Intent(cart_design_And_Chackout.this,inventorysoftwarepackeg.class));
            customType(cart_design_And_Chackout.this,"fadein-to-fadeout");
            finish();
        }
        if (softwarename.equals("Accounting Management")){
            startActivity(new Intent(cart_design_And_Chackout.this,accountingsoftware.class));
            customType(cart_design_And_Chackout.this,"fadein-to-fadeout");
            finish();
        }
        if (softwarename.equals("Real Estate Management")){
            startActivity(new Intent(cart_design_And_Chackout.this,RealEstateManagement.class));
            customType(cart_design_And_Chackout.this,"fadein-to-fadeout");
            finish();
        }
        if (softwarename.equals("HR / Payroll Management")){
            startActivity(new Intent(cart_design_And_Chackout.this,hrpayroll_Managment.class));
            customType(cart_design_And_Chackout.this,"fadein-to-fadeout");
            finish();
        }
        if (softwarename.equals("Garments ERP Management")){
            startActivity(new Intent(cart_design_And_Chackout.this,garments_Management.class));
            customType(cart_design_And_Chackout.this,"fadein-to-fadeout");
            finish();
        }
        if (softwarename.equals("Hospital Management")){
            startActivity(new Intent(cart_design_And_Chackout.this,HospitalManagement.class));
            customType(cart_design_And_Chackout.this,"fadein-to-fadeout");
            finish();
        }
        if (softwarename.equals("Asset / Rent Management")){
            startActivity(new Intent(cart_design_And_Chackout.this,Asset_Managmeant.class));
            customType(cart_design_And_Chackout.this,"fadein-to-fadeout");
            finish();
        }*/
        finish();


    }
}
