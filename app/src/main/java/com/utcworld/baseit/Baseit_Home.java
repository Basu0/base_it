package com.utcworld.baseit;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.fonts.FontFamily;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;

import android.view.Menu;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

import me.itangqi.waveloadingview.WaveLoadingView;
import technolifestyle.com.imageslider.FlipperLayout;
import technolifestyle.com.imageslider.FlipperView;

import static maes.tech.intentanim.CustomIntent.customType;

public class Baseit_Home extends AppCompatActivity implements OnMapReadyCallback , NavigationView.OnNavigationItemSelectedListener {
    SliderLayout sliderLayout1;
    FlipperLayout flipperm;
    LinearLayout layoutcourcs1,layoutcourcs2,layoutmain1,layoutmain2;
    Animation uptodown,downtoup;
    public BottomSheetBehavior mbottomSheetBehavior;
    private ToggleButton tbUpDown;
    GoogleMap map;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baseit__home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setTitle("Base It");
        setSupportActionBar(toolbar);
        layoutcourcs1=findViewById(R.id.courchlayout1);
        layoutcourcs2=findViewById(R.id.courchlayout2);
        layoutmain1=findViewById(R.id.layoutmain1);
        layoutmain2=findViewById(R.id.layoutmain2);
        View bottonshit=findViewById(R.id.botton_shit);
        tbUpDown = findViewById(R.id.toggleButton);
        sliderLayout1 = findViewById(R.id.imageSlider1);
        sliderLayout1.setIndicatorAnimation(IndicatorAnimations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout1.setScrollTimeInSec(1);
        flipperm=findViewById(R.id.flipper);


        uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodowndiagonal);
        downtoup = AnimationUtils.loadAnimation(this,R.anim.uptodowndiagonal);
        layoutmain1.setAnimation(uptodown);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

       mbottomSheetBehavior=BottomSheetBehavior.from(bottonshit);
       // mbottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);


        tbUpDown.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mbottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }else{
                    mbottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                }
            }
        });


  mbottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
      String state = "";
      @Override
      public void onStateChanged(@NonNull View view, int i) {
          if (i == BottomSheetBehavior.STATE_EXPANDED) {
              tbUpDown.setChecked(true);

          } else if (i == BottomSheetBehavior.STATE_COLLAPSED) {
              tbUpDown.setChecked(false);
          }

      }
      @Override
      public void onSlide(@NonNull View view, float v) {

      }
  });

        //layoutcourcs2.setAnimation(downtoup);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        setSliderViews();
        setslidlayout2();




        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.baseit__home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("ResourceType")
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodowndiagonal);
            layoutmain2.setVisibility(View.GONE);
            layoutmain1.setVisibility(View.VISIBLE);
            layoutmain1.setAnimation(uptodown);

        }
        else if (id == R.id.accountid) {
            startActivity(new Intent(Baseit_Home.this,LoginActivity.class));
            customType(Baseit_Home.this,"fadein-to-fadeout");

            finish();
        }

        else if (id == R.id.aboutus) {
            startActivity(new Intent(Baseit_Home.this,aboutus.class));
            customType(Baseit_Home.this,"fadein-to-fadeout");

        } else if (id == R.id.websiteid) {
         uptodown = AnimationUtils.loadAnimation(this,R.anim.uptodowndiagonal);
          layoutmain1.setVisibility(View.GONE);
          layoutmain2.setVisibility(View.VISIBLE);
          layoutmain2.setAnimation(uptodown);


        } else if (id == R.id.software) {
            startActivity(new Intent(Baseit_Home.this,softwarePackeg_Home_page.class));
            customType(Baseit_Home.this,"fadein-to-fadeout");
            //finish();

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.onlinemarkitingid) {
            startActivity(new Intent(Baseit_Home.this,onlinemarkiting_deshboard.class));
            customType(Baseit_Home.this,"fadein-to-fadeout");

        }
     else if (id == R.id.traning) {
            startActivity(new Intent(Baseit_Home.this,traningpage.class));
            customType(Baseit_Home.this,"fadein-to-fadeout");

    }
        else if (id == R.id.payment) {
            startActivity(new Intent(Baseit_Home.this,payment.class));
            customType(Baseit_Home.this,"fadein-to-fadeout");

        }
        else if (id == R.id.career) {
            startActivity(new Intent(Baseit_Home.this,career_findjob.class));
            customType(Baseit_Home.this,"fadein-to-fadeout");

        }
        else if (id == R.id.contact) {
            startActivity(new Intent(Baseit_Home.this,contactForm.class));
            customType(Baseit_Home.this,"fadein-to-fadeout");

        }



        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setSliderViews() {

        for (int i = 0; i <= 3; i++) {

            DefaultSliderView sliderView = new DefaultSliderView(this);

            switch (i) {
                case 0:
                    sliderView.setImageDrawable(R.drawable.webdesignpic);
                    //sliderView.setDescription("WebSite Design & Development");

                    //sliderView.setImageUrl("https://images.pexels.com/photos/547114/pexels-photo-547114.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
                    break;
                case 1:
                    sliderView.setImageDrawable(R.drawable.domainhosting);
                    //sliderView.setDescription("Domain & Hosting Registration");
                    //sliderView.setImageUrl("https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
                    break;
                case 2:
                    sliderView.setImageDrawable(R.drawable.softwaredevelopment);
                    //sliderView.setDescription("Software Development");
                    //sliderView.setImageUrl("https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260");
                    break;
                case 3:
                    sliderView.setImageDrawable(R.drawable.emailmarkitingpic);
                    //sliderView.setDescription("Email Marketing");
                    //sliderView.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
                    break;

        }

            sliderView.setImageScaleType(ImageView.ScaleType.FIT_XY);

            final int finalI = i;
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                   // Toast.makeText(Baseit_Home.this, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
                }
            });

            //at last add this view in your layout :
            sliderLayout1.addSliderView(sliderView);
        }

    }

  public void setslidlayout2(){

        for (int i=0;i<3;i++) {
            FlipperView view=new FlipperView(getBaseContext());
            //view.setImageUrl(ima[i]);
            switch (i) {
                case 0:
                    view.setImageDrawable(R.drawable.liongroup);


                    //sliderView.setImageUrl("https://images.pexels.com/photos/547114/pexels-photo-547114.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
                    break;
                case 1:
                    view.setImageDrawable(R.drawable.shishuhospital);
                    //sliderView.setImageUrl("https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
                    break;
                case 2:
                    view.setImageDrawable(R.drawable.incorporation);
                    //sliderView.setImageUrl("https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260");
                    break;


        }
            view.setImageScaleType(ImageView.ScaleType.FIT_XY);
            flipperm.addFlipperView(view);

        }



}




    public void dynamic_website_order(View view) {
        startActivity(new Intent(Baseit_Home.this,DynamicWebsite_order_page.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
        //finish();
    }

    public void softwarepackegpage(View view) {
        startActivity(new Intent(Baseit_Home.this,softwarePackeg_Home_page.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
        //finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        //close();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        LatLng location=new LatLng(23.816669, 90.366566);
        map.addMarker(new MarkerOptions().position(location).title("BASE IT"));
        map.moveCamera(CameraUpdateFactory.newLatLng(location));
        map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        UiSettings uiSettings = map.getUiSettings();
        uiSettings.setZoomGesturesEnabled(true);
        uiSettings.setZoomControlsEnabled(true);

        //map.animateCamera(CameraUpdateFactory.zoomOut());
        //CameraUpdateFactory.scrollBy(15, 200);
        //map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);


    }

    public void domainpage(View view) {
        startActivity(new Intent(Baseit_Home.this,Domain_regestioton.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
        //finish();
    }

    public void vpspage(View view) {
        startActivity(new Intent(Baseit_Home.this,VPS_Hosting_page.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
        //finish();
    }

    public void dedicatedhostpage(View view) {
        startActivity(new Intent(Baseit_Home.this,DedicatedHosting.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
        //finish();
    }

    public void daynamicwebsite(View view) {
        startActivity(new Intent(Baseit_Home.this,DynamicWebsite_order_page.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
        //finish();
    }

    public void softwarepackegpagego(View view) {
        startActivity(new Intent(Baseit_Home.this,softwarePackeg_Home_page.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
        //finish();
    }

    public void jobprotan(View view) {
        startActivity(new Intent(Baseit_Home.this,jobprotal.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
        //finish();
    }

    public void searchengin(View view) {
        startActivity(new Intent(Baseit_Home.this,SEARCHENGINEOPTIMIZATIO.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }

    public void mobileapps(View view) {
        startActivity(new Intent(Baseit_Home.this,mobileapps.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }

    public void webhostinggo(View view) {
        startActivity(new Intent(Baseit_Home.this,DedicatedHosting.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }

    public void domainpagego(View view) {
        startActivity(new Intent(Baseit_Home.this,Domain_regestioton.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }

    public void onlinemarkiting(View view) {
        startActivity(new Intent(Baseit_Home.this,onlinemarkiting_deshboard.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }

    public void mobileappspage(View view) {
        startActivity(new Intent(Baseit_Home.this,mobileapps.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }

    public void logodesign(View view) {
        startActivity(new Intent(Baseit_Home.this,logodesig.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }

    public void newspagego(View view) {
        startActivity(new Intent(Baseit_Home.this,newspaperpage.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }

    public void e_commeracpage(View view) {
        startActivity(new Intent(Baseit_Home.this,E_commercesite.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }

    public void facebooksite(View view) {
        Intent openURL = new Intent(android.content.Intent.ACTION_VIEW);
        openURL.setData(Uri.parse("https://www.facebook.com/Baseitsloutionsltd/"));
        startActivity(openURL);
    }

    public void twitter(View view) {
        Intent openURL = new Intent(android.content.Intent.ACTION_VIEW);
        openURL.setData(Uri.parse("https://www.facebook.com/Baseitsloutionsltd/"));
        startActivity(openURL);
    }

    public void googleplus(View view) {
        Intent openURL = new Intent(android.content.Intent.ACTION_VIEW);
        openURL.setData(Uri.parse("https://www.facebook.com/Baseitsloutionsltd/"));
        startActivity(openURL);
    }

    public void linkdin(View view) {
        Intent openURL = new Intent(android.content.Intent.ACTION_VIEW);
        openURL.setData(Uri.parse("https://www.facebook.com/Baseitsloutionsltd/"));
        startActivity(openURL);
    }

    public void youtubee(View view) {
        Intent openURL = new Intent(android.content.Intent.ACTION_VIEW);
        openURL.setData(Uri.parse("https://www.facebook.com/Baseitsloutionsltd/"));
        startActivity(openURL);
    }

    public void e_commeracpagego(View view) {
        startActivity(new Intent(Baseit_Home.this,E_commercesite.class));
        customType(Baseit_Home.this,"fadein-to-fadeout");
    }
}
