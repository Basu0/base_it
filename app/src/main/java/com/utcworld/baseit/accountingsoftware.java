package com.utcworld.baseit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class accountingsoftware extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountingsoftware);
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(accountingsoftware.this,softwarePackeg_Home_page.class));
            //customType(accountingsoftware.this,"fadein-to-fadeout");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void accountCartout(View view) {
        Intent intent=new Intent(accountingsoftware.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","Accounting Management");
        intent.putExtra("prices","150000");
        startActivity(intent);
        //finish();
    }

    public void backsoftwarepackeg(View view) {
        //startActivity(new Intent(accountingsoftware.this,softwarePackeg_Home_page.class));
        //customType(accountingsoftware.this,"fadein-to-fadeout");

        finish();
    }
}
