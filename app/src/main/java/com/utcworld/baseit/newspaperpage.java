package com.utcworld.baseit;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.Toolbar;

public class newspaperpage extends AppCompatActivity {
    String webaddress="https://www.baseit.com.bd/news-paper-website/";
    WebView webView;
    FrameLayout frameLayout;
    ProgressBar progressBar;
    LinearLayout layoutwb,layoutapp;
    Toolbar tolbar1,toolbar2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newspaperpage);
        webView = (WebView) findViewById(R.id.webviewid);
        frameLayout = (FrameLayout) findViewById(R.id.framlaout);
        progressBar = (ProgressBar) findViewById(R.id.progressbaar);
        layoutwb=findViewById(R.id.layoutweb);
        layoutapp=findViewById(R.id.layoutapps);
        tolbar1=findViewById(R.id.toolbarmain);
        toolbar2=findViewById(R.id.toolbarweb);
        assert webView != null;
        layoutapp.setVisibility(View.VISIBLE);

        WebSettings webSettings = webView.getSettings();
        webSettings.setAllowFileAccess(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setJavaScriptEnabled(true);
    }

    public void sitego(View view) {
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                tolbar1.setVisibility(View.GONE);
                toolbar2.setVisibility(View.VISIBLE);
                layoutwb.setVisibility(View.VISIBLE);
                layoutapp.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
                progressBar.setProgress(newProgress);
                setTitle("Loading Please Wait.... ");
                if (newProgress==100){
                    frameLayout.setVisibility(View.GONE);
                    setTitle(view.getTitle());

                }
                super.onProgressChanged(view, newProgress);
            }




        });



        /*if (havenetworkConnecation()){
            webView.loadUrl(webaddress);

        }*/
        boolean wificonneaction;
        boolean mobileconneaction;
        ConnectivityManager connectivityManager=(ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();
        if (networkInfo !=null && networkInfo.isConnected()){
            wificonneaction=networkInfo.getType()==ConnectivityManager.TYPE_WIFI;
            mobileconneaction=networkInfo.getType()==ConnectivityManager.TYPE_MOBILE;
            if (wificonneaction){

                webView.loadUrl(webaddress);
            }
            else if (mobileconneaction){
                webView.loadUrl(webaddress);


            }}

        else {

            LayoutInflater layoutInflater=LayoutInflater.from(this);
            View vv=layoutInflater.inflate(R.layout.no_net_alartdilog,null);
            Button button=vv.findViewById(R.id.exit_bt);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //moveTaskToBack(true);
                    //android.os.Process.killProcess(android.os.Process.myPid());
                    //System.exit(1);
                    finish();
                }
            });
            AlertDialog alertDialog=new AlertDialog.Builder(this)
                    .setView(vv)
                    .create();
            alertDialog.show();
            //Toast.makeText(this, "No Internet Connecation ... ", Toast.LENGTH_SHORT).show();

        }

        progressBar.setProgress(0);
    }


    public void profisanacart(View view) {
        Intent intent=new Intent(newspaperpage.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","News Paper – Professional Package");
        intent.putExtra("prices","150000");
        startActivity(intent);
    }

    public void standercart(View view) {
        Intent intent=new Intent(newspaperpage.this,cart_design_And_Chackout.class);
        intent.putExtra("softwarename","News Paper – STANDARD package");
        intent.putExtra("prices","70000");
        startActivity(intent);
    }

    public void backsoftwarepackeg(View view) {
        toolbar2.setVisibility(View.GONE);
        tolbar1.setVisibility(View.VISIBLE);
        layoutwb.setVisibility(View.GONE);
        layoutapp.setVisibility(View.VISIBLE);

    }

    public void toolbarmin(View view) {
        finish();
    }
    private boolean havenetworkConnecation(){
        boolean haveconnectionWifi=false;
        boolean haveconnectionMobile=false;
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos=cm.getAllNetworkInfo();
        for (NetworkInfo ni: networkInfos){
            if (ni.getTypeName().equalsIgnoreCase("WiFi"))
                if (ni.isConnected())
                    haveconnectionWifi=true;
            if (ni.getTypeName().equalsIgnoreCase("Mobile"))
                if (ni.isConnected())
                    haveconnectionMobile=true;
        }
        return haveconnectionWifi || haveconnectionMobile;

    }
}
